-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2020 at 07:24 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `map_linh_lang`
--

-- --------------------------------------------------------

--
-- Table structure for table `map_place`
--

CREATE TABLE `map_place` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(256) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `image_name` varchar(256) NOT NULL,
  `coordinate` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `map_place`
--

INSERT INTO `map_place` (`id`, `name`, `address`, `content`, `image_name`, `coordinate`) VALUES
(39, 'Đền Voi Phục', '251 Thụy Khuê, Thuỵ Khuê, Tây Hồ, Hà Nội', 'Theo lịch sử truyền lại, ngày 13 tháng Chạp năm Kỷ tỵ (1030). Thứ phi của vua Lý Thái Tông là Bà Hạo Nương hạ sinh được một hoàng tử khôi ngô, tuấn tú trong một làng nhỏ thuộc phường Thụy Chương ( nay là phường Thụy Khê) ở ven hồ Dâm Đàn ( tức Hồ Tây). Đức Vua vui mừng đặt tên Hoàng tử là Linh Lang (hay còn gọi là Hoàng Chân).\r\nNgay từ khi còn nhỏ, Linh Lang đã tài trí hơn người, Ngài cùng các huynh đệ chăm chỉ học hành, võ thuật. Năm 14 tuổi, nhờ có võ nghệ cao cường , Hoàng tử đã được vua cha dẫn đi đánh giặc Chiêm Thành, đuổi giặc tới thành Đồ Bàn ( Quy Nhơn – Bình Định).\r\nTiếp đó, ngài cùng anh trai là vua Lý Thánh Tông tiến quân về phía Nam Hải (Bến Hải Quang Trị) đánh bại quân Vĩnh Trịnh năm 1069.\r\nNhà Tống đem quân sang xâm lược năm 1076 – 1077. Tướng Lý Thường Kiệt tổng chỉ huy mọi lực lượng bộ binh, kỵ binh, thủy binh. Hoàng tử Linh Lang được Lý Thường Kiệt tin tưởng giao cho đảm nhận lực lượng thủy quân từ Vạn Xuân đánh ngược lên phía Bắc. Trận đánh đã tiêu diệt cụm quân của tránh tướng Quách Quỳ và tiêu hao khá nhiều vũ khí của địch. Sau đó, ngài phối hợp với đạo quân của Lý Thường Kiệt phản công mạnh mẽ vào lực lượng của phó tướng giặc là Triệu Tiết khiến cho quân Tống bị thương vong vô số. Lúc này, do quân địch bị tiêu hao quá nhiều sức ngừơi sức của, không thể chống đỡ được trên tuyến sông Như Nguyệt ( nay thuộc tỉnh Bắc Ninh) nên giặc phải xin hòa và rút quân về nước.\r\nHoàng tử Linh Lang đã chiến đấu vô cùng anh dũng, ngoan cường và mưu lược trong trận đánh liệt này. Tuy đánh bại kẻ thù nhưng sau đó Ngài đã hi sinh. Nhà Vua biết tin vô cùng thương tiếc và đau lòng trước sự ra đi đột ngột của Hoàng tử. Sau đó, Vua ban sắc phong Hoàng tử Linh Lang là Linh Lang Đại vương thượng đẳng, tối linh thần truyền cho tất cả những nơi Linh Lang đã từng đi qua đều phải lập đền thờ để tưởng nhớ công lao của ông', '20200429_140347_den_voi_phuc.jpg', '105.815469, 21.043786'),
(62, 'Đình Nhật Tân', '401 Âu Cơ, Nhật Tân, Tây Hồ, Hà Nội, Việt Nam', 'Đức Thánh Uy Đô Đại Vương Trần Linh Lang (Uy Linh Lang) là con bà Chính Cung Minh Đức Hoàng Hậu, Uy Linh Lang là người nổi tiếng thông minh, văn võ song toàn, xa gần đều biết tiếng. Đức Thánh Uy Đô Đại Vương Trần Linh Lang (Uy Linh Lang) giáng kiệu.\r\nĐến thời Trần Nhân Tông, giặc Nguyên Mông đem 40 vạn quân, do tướng giặc là Toa Đô cầm đầu tiến đánh nước ta. Ông dâng biểu, xin vua cha được đi đánh giặc và viết bài hịch truyền kêu gọi, được nhân dân nô nức hưởng ứng. Ông thành lập đội quân xưng là “Thiên tử quân”, tiến đánh quân Nguyên và đại thắng. Giặc tan, Vua phong Ông là Dâm Đàm Đại Vương (Đại Vương Hồ Tây). Ông mất vào giờ Ngọ ngày 8/8 năm Canh Tý (1300). Vua thương tiếc cho xây đền tại chỗ ông mất để nhân dân hương khói phụng thờ và gọi là đền Nhật Chiêu, sắc phong là Hiển Minh Đức.', '1589731798269.jpg', '105.82149372541447, 21.076182274'),
(63, 'Đền Thủ Lệ', '306B Kim Mã, Ngọc Khánh, Ba Đình, Hà Nội, Việt Nam', 'Hoàng tử Linh Lang, con của vua Lý Thái Tông, và bà phi thứ 9 Dương Thị Quang, nhưng tương truyền vốn là con của Long Quân, tên gọi là Hoàng Châu, thác sinh, là người có công trong cuộc kháng chiến chống quân xâm lược nhà Tống, và đã hi sinh trên phòng tuyến sông Cầu vào năm 1076.\r\nSau khi mất, được người dân Thủ Lệ lập đền thờ và được nhà vua sắc phong là Linh Lang đại vương thượng đẳng phúc thần. Thần đã nhiều lần âm phù giúp nhà Trần trong cuộc chiến chống quân xâm lược Nguyên - Mông, và nhà Lê trong cuộc phục hưng. Vì trước cửa đền có đắp hai con voi quỳ gối nên quen gọi là đền Voi Phục và vì đền ở phía tây kinh thành nên còn gọi là trấn Tây hoặc trấn Đoài (Đoài, theo bát quái thuộc phương Tây).', '1589731865489.jpg', '105.80451589657788, 21.029282700'),
(64, 'Đình Cổ Vũ', '85 Hàng Gai, Hoàn Kiếm, Hà Nội, Việt Nam', 'Linh Lang là con thứ tư của Lý Thánh Tông, mẹ là Cảo Nương, người làng Bồng Lai, huyện Từ Liêm ( Nay thuộc huyện Đan Phượng, Hà Tây ) ngụ ở Thị Trại ( Trại Chợ ). Tuy là cung phi nhưng bà vẫn được về ở nhà riêng. Một lần Cảo Nương đi tắm ở hồ Tây bị rồng cuốn lấy người. Sau đó Cảo Nương có mang và sinh ra Linh Lang\r\nKhi quân Tống xâm lược nước ta, vua cho sứ đi cầu hiền. Linh Lang liền xin vua cấp cho một cỗ voi và một cây cờ hồng cán dài mười trượng để đi dẹp giặc. Được vua chấp thuận, Linh Lang cưỡi voi, cầm cờ ra trận và đại thắng. Vua muốn nhường ngôi nhưng chàng chối từ, trở về ở Trại Chợ. Ít lâu sau chàng bị bệnh đậu mùa rồi từ trần, hoá ra rồng đen đi xuống hồ Tây. Vua cho lập đền Voi Phục ( Thủ Lệ ) ngay ở nơi chành Linh Lang hoá rồng và phong thần.', '1589725705767.jpg', '105.84982240823454, 21.031639211'),
(65, 'Đình Đồng Lạc', '38 Hàng Đào, Hoàn Kiếm, Hà Nội, Việt Nam', 'Thờ cúng Linh Lang Đại Vương', '1589732073830.jpg', '105.85088979977364, 21.033479413'),
(66, 'Đình Vĩnh Phúc Hạ', 'Hoàng Hoa Thám, Vĩnh Phú, Ba Đình, Hà Nội, Việt Nam', 'Thờ Linh Lang Đại Vương', '1589732529999.png', '105.8117415706519, 21.0445314191'),
(67, 'Đình Yên Phụ', 'Yên Phụ, Tây Hồ, Hà Nội, Việt Nam', 'Uy Linh Lang là con trai của hoàng hậu Minh Đức, dưới thời Trần Thánh Tông ( trị vì từ 1258 – 1278). Uy Linh Lang được chăm sóc chu đáo, hay ăn, chóng lớn. Đến khi đi học, ông thông minh lanh lợi khác người. Ở tuổi 18, ông ham mê đạo Phật, xin phép vua cha cho xuất gia nhưng không được chấp thuận. Uy Linh Lang bèn thay áo, giả làm dân thường, trốn đi tìm thầy học đạo. Mới học được vài tháng đã thông hiểu nhiều kinh sách nhà Phật, được nhiều người biết tiếng. Vua cha triệu Uy Linh Lang về kinh đô, cho ở trại Bình Thọ (tứ Yên Hoa), hàng tháng cấp lương bổng để tĩnh tâm tu luyện.\r\nKhi Uy Linh Lang tròn 20 tuổi, quân Nguyên Mông kéo sang xâm lược nước Đại Việt lần thứ ba. Căm thù giặc người người đều trích vào cánh tay hai chữ “Sát thát” tỏ ý chí giết giặc cứu nước. Trước tình thế đó, Uy Linh Lang nói: “Làm trai sinh ra giữa trời đất, nếu không dẹp giặc cứu đời thì sao có tên lưu lại trong sử sách”. Rồi ông viết bài biểu dâng lên vua xin được cầm quân đánh giặc. Nhà vua chuẩn y.\r\nUy Linh Lang dựng cờ chiêu mộ binh sĩ, chỉ trong vài ngày đã có hàng nghìn người theo ông luyện tập quân sự, học binh pháp, tổ chức đội ngũ chỉnh tề. Đội quân của ông tự xưng là “Thiên tử quân”, tiến đánh quân Nguyên Mông ở Bàn Than, Vạn Kiếp, Mạn Trù, Đông Kết… lập được nhiều chiến công. Khi bình công, xét thưởng, Uy Linh Lang được vua phong là Dâm Đàm Đại Vương. Ông không màng danh lợi mà ở lại chùa Ngọc Hồ để tu thiền. Giờ ngọ ngày 8/8 năm Bính Tý, Đại Vương không bệnh mà hóa. Vua ban cho xây đền thờ ông ở Nhật Chiêu (nay là Nhật Tân) và các nơi khác như ở Yên Hoa (Yên Phụ).\r\nNhững nơi ông đi qua nhân dân nhớ và lập đình thờ phong Thành Hoàng làng', '1589732164982.jpg', '105.83612116461298, 21.052474534'),
(68, 'Đình Yên Thịnh', '12 ngõ Chùa Vua', 'Thờ cúng Linh Lang Đại Vương', '', '105.853035, 21.011912'),
(69, 'Đình Kim Mã Thượng', 'Ngõ 294 - Đội Cấn, Ba Đình, Hà Nội', 'Theo truyền thuyết ông là hoàng tử, con trai thứ 4 của vua Lý Thánh Tông và bà cung phi Cảo Nương. Một lần Cảo Nương đi tắm ở Hồ Tây, bị rồng quấn lấy người sau đó liền mang thai và sinh ra Linh Lang. Khi lớn lên gặp lúc giặc Tống xâm lược bờ cõi ông xin vua cha cấp một cỗ voi và một cây cờ hồng để đi dẹp giặc. Ông dùng cờ lệnh, lệnh cho voi quỳ xuống rồi xông ra trận, cờ lệnh ông chỉ đến đâu giặc tan đến đấy. Với chiến thắng vang dội vua cha muốn nhường ngôi nhưng ông không nhận, ông chỉ thích sống cuộc đời bình dị. Ít lâu sau ông bị bệnh rồi mất. Khi ông mất hóa thành giao long bò xuống hồ Tây rồi biến mất. Vua xót thương bèn sai lập đền thờ tại nơi thần hóa và phong thần là Linh Lang Đại Vương. Ông được suy tôn là vị thần trấn giữ, bảo vệ phía tây kinh thành Thăng Long. Gần đây, các nhà sử học cho rằng Linh Lang chính là hình ảnh được thần linh hoá của Hoàng tử Hoàng Chân —người đã hi sinh anh dũng trong cuộc kháng chiến chống Tống tại bờ sông Như Nguyệt năm 1076.', '1589732257693.jpg', '105.81316980604888, 21.037953889'),
(70, 'Đình Hào Nam', 'Vũ Thạnh, Chợ Dừa, Đống Đa, Hà Nội, Việt Nam', 'Thờ Linh Lang Đại Vương', '1589732306440.jpg', '105.82664013735184, 21.027348014'),
(71, 'Đình Vạn Phúc', 'Ngõ 194 Đội Cấn, Ba Đình, Hà Nội, Việt Nam', '(Linh Lang Đại Vương có tên là Hoàng Chân, con trai thứ tư của Vua Lý Thánh Tông ( trị vì 1032 – 1072), mẹ là cung phi thứ 9, quê ở Đồng Đoàn xã Bồng Lai, Đan Phượng, trấn phương Tây. Năm 1972, Lý Thánh Tông mất, Lý Càn Đức mới 7 tuổi lên nối ngôi, tức là vua Lý Nhân Tông.\r\nNăm 1075 nước có nạn ngoại xâm, Linh Lang xin nhà vua cùng Hoàng tử Chiêu Văn chỉ huy Hạm thuyền vượt qua biển Vĩnh An, tiến công, đập tan các đồn bốt giặc, phối hợp cùng các đạo quân của tướng Tôn Đản đánh chiếm cơ sở chiến lược, tập trung lương thảo của giặc Tống xâm lược. Quân ta đại thắng, nhà Vua mở Đại yến, muốn nhường ngôi cho Hoàng tử nhưng Người từ chối. Đất nước thanh bình được một thời gian, đến cuối năm 1076 giặc Tống lại đưa quân sang xâm lược nước ta, một lần nữa Hoàng tử lại cùng Hoàng tử Chiêu Văn chỉ huy Hạm thuyền từ Vạn Xuân ngược dòng Kháo Túc, bất ngờ tập kích vào phòng tuyến phía Đông của giặc Tống bên bờ bắc sông Như Nguyệt, góp phần đánh đuổi giặc Tống khỏi biên cương của Tổ Quốc.\r\nTại dòng sông này Hoàng tử đã hy sinh anh dũng (mồng 10 tháng 02 Đinh Tỵ – 1077). Xét công trạng của hoàng tử nhà Vua ban phong Mỹ tự, cho phép 269 làng trại trong cả nước xây đền miếu thờ cùng sắc phong Linh Lang Đại Vương Thượng Đẳng Phúc Thần.)', '1589731334388.jpg', '105.82406077308143, 21.033938704'),
(72, 'Đình Yên Xá', '121 Yên Xá, Tân Triều, Thanh Trì, Hà Nội, Việt Nam', '(Uý Đô Linh Lang, tương truyền là con trai của Vua Trần Thánh Tông (..... - 1278) và Hoàng hậu Minh Đức. Uý Đô là người không ham cuộc sống quyền chức mà đi tu ở nhiều chùa. Đến khi giặc Nguyên - Mông xâm lược nước ta, Vua Trần Thánh Tông cho gọi Uý Đô về trông coi Kinh thành Thăng Long, sau Uý Đô lại ra trận, tổ chức tuyến phòng thủ ở nhiều làng xã phía Nam Kinh thành Thăng Long, trong đó có làng Yên Xá, làng Lưu Phái (nay thuộc xã Ngũ Hiệp cùng thuộc huyện Thanh Trì) lập công, nên được phong làm Linh Lang đại vương, ban thực ấp ở vùng Dâm Đàm (Hồ Tây). Sau khi ông mất, các làng là nơi ông tổ chức phòng tuyến và làng trong khu thực ấp của ông đều lập đền thờ, đền chính ở phường  Nhật Chiêu)', '1589731421972.jpg', '105.79644161879439, 20.969369640'),
(73, 'Đình Nội', 'làng Bình Đà, Quốc lộ 21B, Bình Đà, Bình Minh, Thanh Oai, Hà Nội', 'Ngôi đình Ngoại thờ Linh Lang đại vương, hoàng tử nhà Lý con vua Lý Thánh Tông, người hy sinh trong chiến tranh chống Tống', '1589732348659.jpg', '105.76682389657529, 20.894628745'),
(74, 'Đình Xa La', 'P. Phúc La, Hà Đông, Hà Nội, Việt Nam', 'Thờ Linh Lang Đại vương tức Hoàng tử Hoàng Chân triều Lý có công lớn trong cuộc kháng chiến chống quân Tống xâm lược thế kỷ thứ XI. Hiện nhân dân còn giữ đựơc 10 đạo sắc phong. Đạo sắc phong sớm nhất vào thời Cảnh Hưng  năm thứ 44 ( năm 1783, thuộc triều vua Lê Hiển Tổng)', '1589733556369.jpg', '105.78868714216655, 20.964303292'),
(75, 'Đình Ngọc Khánh', 'Ngọc Khánh, Ba Đình, Hà Nội, Việt Nam', 'Linh Lang - vị Hoàng tử có công đánh giặc Tống trên trận tuyến sông Như Nguyệt (sông Cầu) vào năm 1076', '1589733640257.jpg', '105.81180662520859, 21.028989400');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `token` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `password`, `token`) VALUES
(1, 'ngoctan1294', '$2y$07$1GXv76FV9yVhZK76KQ0LzePgDGVQcsLtQajL4D5vVaer.FewUJhsS', ''),
(2, 'lananh.tang', '$2y$07$IhxxvTu1cGI4q4r6BiNhPOqygRIOl6aV5r.Wih4Zqo/z9s7r63w3u', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `map_place`
--
ALTER TABLE `map_place`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `map_place`
--
ALTER TABLE `map_place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
