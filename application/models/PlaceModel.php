<?php

class PlaceModel extends CI_Model
{
  private $table = 'map_place';
  public function __construct()
  {
    parent::__construct();
  }
  public function get_data()
  {
    $this->db->select('*');
    $query = $this->db->get($this->table);
    return $query->result();
  }
}