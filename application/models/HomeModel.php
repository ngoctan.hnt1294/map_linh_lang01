<?php

class HomeModel extends CI_Model
{
  private $table = 'map_place';
  public function __construct()
  {
    parent::__construct();
  }
  public function get_data()
  {
    $this->db->select('name, address, image_name');
    $query = $this->db->get($this->table, 5, 0);
    return $query->result();
  }
}