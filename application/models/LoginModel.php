<?php

class LoginModel extends CI_Model
{
  private $userTable = 'user';
  public function __construct()
  {
    parent::__construct();
  }
  public function check_login($user_name)
  {
    $query = $this->db->get_where($this->userTable, array('user_name' => $user_name));
    return ($query->row());
  }
}