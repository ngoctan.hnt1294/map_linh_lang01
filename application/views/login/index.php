<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap-reboot.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/plugin/fontawesome/css/all.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/css/login/style.css") ?>">
</head>
<body>
  <div class="login-wrapper">
    <div class="login-content">
      <h1 class="login__ttl">Sign In</h1>
      <form action="" class="login-form" id="login_form">
        <p class="js-failLogin fail-login">
          <i class="fas fa-times"></i><span></span>
        </p>
        <div class="form-group">
          <label for="user_name">Username</label>
          <input type="text" name="user_name" id="user_name" placeholder="Nhập username">
          <div class="error_alert js-errorAlert"></div>
        </div>
        <div class="form-group">
          <label for="user_name">Password</label>
          <input type="password" name="password" id="password" placeholder="Nhập password">
          <div class="error_alert js-errorAlert"></div>
        </div>
        <div class="">
          <input type="checkbox" name="remember_me" id="remember_me">
          <label for="remember_me">Remember me</label>
        </div>
        <div class="form-group">
          <button type="submit" class="btn submit-btn js-submitLogin">Sign in</button>
        </div>
      </form>
    </div>
  </div>
  <script src="<?= base_url("public/plugin/jquery/jquery.js") ?>"></script>
  <script src="<?= base_url("public/js/login/script.js") ?>"></script>
</body>
</html>