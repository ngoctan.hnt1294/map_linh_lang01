<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Địa điểm thờ cúng Linh Lang Đại Vương</title>
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/css/common/common.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/place/index.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/place/style.css") ?>">
</head>
<body>
  <?php $this->load->view('common/header.inc'); ?>
  <div id="map" class="map"></div>
  <div id="popup" class="ol-popup">
    <a href="#" id="popup-closer" class="ol-popup-closer"></a>
    <div id="popup-content"></div>
  </div>
  <?php $this->load->view('common/footer.inc'); ?>
  <?php $this->load->view('/common/base_url.inc') ?>
  <script src="<?= base_url("public/plugin/jquery/jquery.js") ?>"></script>
  <script src="<?= base_url("public/place/index.js") ?>"></script>
</body>
</html>