<?php
  $userName = $this->session->userdata('user_name');
  if ($userName == null && trim($userName) == '') {
    redirect('/login', 'refresh');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dashboard</title>
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap-reboot.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/plugin/fontawesome/css/all.min.css") ?>">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
  <link rel="stylesheet" href="<?= base_url("public/css/dashboard/style.css") ?>">
</head>
<body>
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-dark border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Dashboard</div>
      <div class="list-group list-group-flush">
        <a href="#" data-target="dashboard" class="js-switch list-group-item list-group-item-action bg-dark">Place</a>
        <a href="#" data-target="post" class="js-switch list-group-item list-group-item-action bg-dark">Post</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <figure>
                  <img src="<?= base_url("public/img/icon_avatar.jpg") ?>" alt="">
                </figure>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"><?= $userName ?></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item js-logout" href="#">Log out</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid js-container">
        <div class="dashboard" id="dashboard">
          <?php $this->load->view('dashboard/dashboard.inc') ?>
        </div><!-- /dashboard -->
        <div class="post" id="post">
          <?php $this->load->view('dashboard/post.inc') ?>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <?php $this->load->view('dashboard/modal.inc') ?>
  <?php $this->load->view('/common/base_url.inc') ?>
  <script src="<?= base_url("public/plugin/jquery/jquery.js") ?>"></script>
  <script src="<?= base_url("public/plugin/bootstrap/js/bootstrap.min.js") ?>"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest"></script>
  <script src="<?= base_url("public/js/dashboard/script.js") ?>"></script>
</body>
</html>