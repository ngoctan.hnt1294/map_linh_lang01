<h1 class="mt-4">Các địa điểm thờ cúng Linh Lang Đại Vương</h1>
<button class="btn btn-primary btn-add js-insert" data-toggle="modal" data-target="#modal_form"><i class="fas fa-plus"></i>Thêm địa điểm</button>
<table id="dashboard-table" class="table table-striped table-bordered dashboard-table">
  <thead>
    <tr>
      <th class="text-center" style="width: 150px">Tên</th>
      <th class="text-center" style="width: 380px">Địa chỉ</th>
      <th class="text-center" style="width: 250px">Tọa độ</th>
      <th class="text-center" style="width: 595px">Nội dung</th>
      <th class="text-center" style="width: 200px">Action</th>
    </tr>
  </thead>
  <tbody id="js-dashboardTbody"></tbody>
</table>