<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title js-modalTitle">Thêm địa điểm</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body form">
        <form action="" id="form_place" class="form-horizontal">
          <input type="hidden" value="" name="id"/> 
          <div class="form-body">
            <div class="form-group">
              <label for="name" class="control-label font-weight-bold">Tên<sup class="text-danger">*</sup></label>
              <input id="name" name="name" placeholder="Nhập địa điểm..." class="form-control" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label for="address" class="control-label font-weight-bold">Địa chỉ <sup class="text-danger">*</sup></label>
              <input id="address" name="address" placeholder="Nhập điạ chỉ..." class="form-control" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label for="coordinate" class="control-label font-weight-bold">Tọa độ <sup class="text-danger">*</sup></label>
              <input id="coordinate" name="coordinate" placeholder="Nhập tọa độ..." class="form-control" type="text">
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label for="content" class="control-label font-weight-bold">Nội dung <sup class="text-danger">*</sup></label>
              <textarea id="content" form="form_place" class="form-control" cols="30" rows="5" name="content" placeholder="Nhập nội dung..." id="content"></textarea>
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group js-imgPreview">
              <label class="control-label font-weight-bold">Photo</label>
              <div class="js-imgStatus">(No photo)</div>
              <div class="form-alert js-alert text-danger"></div>
            </div>
            <div class="form-group">
              <label class="control-label font-weight-bold" id="imgLabel">Upload image</label>
              <input name="image" type="file">
              <div class="form-alert js-alert text-danger"></div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary js-btnSave">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->