<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Home</title>
  <link rel="stylesheet" href="<?= base_url("public/plugin/bootstrap/css/bootstrap.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/plugin/fontawesome/css/all.min.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/css/common/common.css") ?>">
  <link rel="stylesheet" href="<?= base_url("public/css/home/style.css") ?>">
</head>
<body>
  <?php $this->load->view('common/header.inc'); ?>
  
  <section class="mt-4 mb-4">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="overview">
              <h1 class="overview__ttl text-center">Linh Lang Đại Vương</h1>
              <figure class="content-body__img text-center">
                <img src="<?= base_url('public/img/home/than-tich-than-linh-lang.jpg') ?>" alt="">
                <figcaption class="font-italic">Ảnh Linh Lang Đại Vương ở đền Voi Phục(Thụy Khuê)</figcaption>
              </figure>
              <h2>1. Góc nhìn lịch sử</h2>
              <p class="overview__txt">Theo lịch sử truyền lại, ngày 13 tháng Chạp năm Kỷ tỵ (1030). Thứ phi của vua Lý Thái Tông là Bà Hạo Nương hạ sinh được một hoàng tử khôi ngô, tuấn tú trong một làng nhỏ thuộc phường Thụy Chương (nay là phường Thụy Khê) ở ven hồ Dâm Đàn (tức Hồ Tây). Đức Vua vui mừng đặt tên Hoàng tử là Linh Lang (hay còn gọi là Hoàng Chân).
              Ngay từ khi còn nhỏ, Linh Lang đã tài trí hơn người, Ngài cùng các huynh đệ chăm chỉ học hành, võ thuật. Năm 14 tuổi, nhờ có võ nghệ cao cường, Hoàng tử đã được vua cha dẫn đi đánh giặc Chiêm Thành, đuổi giặc tới thành Đồ Bàn (Quy Nhơn – Bình Định).
              Tiếp đó, ngài cùng anh trai là vua Lý Thánh Tông tiến quân về phía Nam Hải (Bến Hải Quang Trị) đánh bại quân Vĩnh Trịnh năm 1069.
              Nhà Tống đem quân sang xâm lược năm 1076 – 1077. Tướng Lý Thường Kiệt tổng chỉ huy mọi lực lượng bộ binh, kỵ binh, thủy binh. Hoàng tử Linh Lang được Lý Thường Kiệt tin tưởng giao cho đảm nhận lực lượng thủy quân từ Vạn Xuân đánh ngược lên phía Bắc. Trận đánh đã tiêu diệt cụm quân của tránh tướng Quách Quỳ và tiêu hao khá nhiều vũ khí của địch. Sau đó, ngài phối hợp với đạo quân của Lý Thường Kiệt phản công mạnh mẽ vào lực lượng của phó tướng giặc là Triệu Tiết khiến cho quân Tống bị thương vong vô số. Lúc này, do quân địch bị tiêu hao quá nhiều sức ngừơi sức của, không thể chống đỡ được trên tuyến sông Như Nguyệt (nay thuộc tỉnh Bắc Ninh) nên giặc phải xin hòa và rút quân về nước.
              Hoàng tử Linh Lang đã chiến đấu vô cùng anh dũng, ngoan cường và mưu lược trong trận đánh liệt này. Tuy đánh bại kẻ thù nhưng sau đó Ngài đã hi sinh. Nhà Vua biết tin vô cùng thương tiếc và đau lòng trước sự ra đi đột ngột của Hoàng tử. Sau đó, Vua ban sắc phong Hoàng tử Linh Lang là Linh Lang Đại vương thượng đẳng, tối linh thần truyền cho tất cả những nơi Linh Lang đã từng đi qua đều phải lập đền thờ để tưởng nhớ công lao của ông</p>
              <p class="font-italic">Ghi chép trong  cuốn Tóm tắt niên biểu lịch sử Việt Nam và trong tấm bia tại Đền Voi Phục ( Thuỵ Khuê – Tây Hồ - Hà Nội)</p>
              <h2>2. Lễ hội Đền Voi Phục</h2>
              <figure class="text-center mt-3">
                <img src="<?= base_url('public/img/home/le_hoi.png') ?>" alt="">
              </figure>
              <p>Đặc điểm: Lễ rước kiệu, lễ tế, múa rồng, múa lân, dâng hương, đấu cờ, đập niêu, chọi gà, biểu diễn văn nghệ...</p>
              <p>Đền Voi Phục thuộc “Thăng Long Tứ Trấn”, thờ Hoàng tử Linh Lang. Linh Lang là hoàng tử thứ 4, con Vua Lý Thánh Tông và cung phi thứ 9 là bà Nạo Nương. Thủa ấy, quân xâm lược nhà Tống kết hợp với quân Chiêm thành xâm chiếm nước ta. Hoàng tử đã xin vua cha được ra trận để dẹp giặc. Vua đồng ý và cấp quân lệnh cho hoàng tử tùy nghi hành động. Với tài thao lược của mình, Hoàng tử Linh Lang đã đánh tan quân giặc, giữ yên được bờ cõi. <br>Nhà vua mở yến tiệc khao thưởng các tướng sĩ và có ý muốn nhường ngôi cho hoàng tử Linh Lang nhưng ngài không nhận. Ít lâu sau, hoàng tử lâm bệnh và mất vào ngày mùng 10 tháng Hai âm lịch. Vua cha thương xót người con có tài, nên sắc phong là “Linh Lang Đại Vương” và cho lập đền thờ để dân chúng hương khói đời đời. </p>
              <p> Lễ hội truyền thống đền Voi Phục được tổ chức hàng năm với sự tham gia của 4 đình gồm: đình Ngọc Khánh, đình Yên Hòa, đình Xa La và đình Hào Nam trong 2 ngày, từ ngày 09 đến 10 tháng Hai âm lịch, để kỷ niệm ngày mất của Hoàng tử Linh Lang. </p>
              <p>Chương trình lễ hội được diễn ra như sau:</p>
              <p>Ngày 09/02:</p>
              <p>Từ sáng sớm, lễ cáo thỉnh Đức thánh được thực hiện bởi cụ Từ (người trông đền), sau đó là khóa tụng kinh và đội tế nam quan đền Voi Phục tế Thánh. Tiếp đến, các đội dâng hương nữ của 4 đình vào dâng hương lễ Thánh. Buổi chiều, các trò hội và biểu diễn văn nghệ truyền thống được tổ chức, nhân dân cùng du khách vào lễ Thánh.</p>
              <p>Ngày 10/02 (Chính hội)</p>
              <p>Từ 7h sáng, 4 kiệu từ 4 đình được rước đến đền Voi Phục để tế và bái yết Thánh với đội hình gồm: đội múa rồng; đội đánh trống, đánh chiêng; đội cầm vũ khí: gươm hầu, bát bửu, chấp kích; đội nhạc lễ, bát âm, đồng văn (trống); đội cấm vệ quân hầu Thánh; đội rước kiệu. Trong lễ rước, các đoàn đều trình diễn các hoạt cảnh mang đậm tính dân gian truyền thống như: con đĩ đánh bồng, múa sênh tiền, múa quạt, múa sư tử, múa lân... Tiếp theo, tại đền Voi Phục, bài diễn văn khai mạc lễ hội và thần phả của Đức Thánh. Sau đó, đội nữ dâng hương đền Voi Phục vào lễ Thánh. Buổi chiều, lễ hội được tiếp tục với màn tế lễ của đội tế nam và đội dâng hương nữ của các đình cùng khách thập phương vào lễ Thánh. Kết thúc là lễ tế hạ hội lúc xế chiều.</p>
              <p class="mb-0">Trong hai ngày diễn ra lễ hội còn có nhiều hoạt động được tổ chức như: thi đấu cờ tướng,  biểu diễn võ thuật, thi chọi gà, đập nồi niêu có thưởng và biểu diễn văn nghệ...</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="sidebar p-3">
              <h3 class="sidebar__ttl text-center pb-3 mb-3">Địa điểm thờ cúng</h3>
              <div class="sidebar-content js-sidebarContent"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php $this->load->view('common/footer.inc'); ?>
  <?php $this->load->view('/common/base_url.inc') ?>
  <script src="<?= base_url("public/plugin/jquery/jquery.js") ?>"></script>
  <script src="<?= base_url("public/js/home/script.js") ?>"></script>
</body>
</html>