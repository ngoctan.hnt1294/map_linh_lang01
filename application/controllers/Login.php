<?php
class Login extends CI_Controller
{
  public $data = [];
  public $message = '';
  public $status = TRUE;

  public function __construct()
  {
    parent::__construct();
    $this->load->library(['form_validation', 'session']);
    $this->load->model('LoginModel', 'login');
  }

  public function index()
	{
    $this->load->view('login/index');
  }
  
  public function check_login()
  {
    if($this->input->method() == 'post')
    {
      $this->_validation();
      if ($this->status == TRUE) {
        $data = [
          'user_name' => $this->input->post('user_name'),
          'password' => $this->input->post('password'),
          'remember_me' => $this->input->post('remember_me')
        ];
        $user = $this->login->check_login($data["user_name"]);
        if(empty($user)) 
        {
          $this->_loginFail();
        } else
        {
          if(password_verify($data['password'], $user->password))
          {
            $this->status = TRUE;
            $this->message = "Đăng nhập thành công";
            $this->session->set_userdata($data);
          }
          else
          {
            $this->_loginFail();
          }
        }
      }
    }
    
    $this->_response();
  }

  public function logout() {
    $this->session->unset_userdata('user_name');
    $this->session->sess_destroy();
  }

  private function _response()
  {
    echo json_encode(
      [
        'status' => $this->status,
        'message' => $this->message,
        'data' => $this->data
      ],
      JSON_UNESCAPED_UNICODE
    );
  }

  private function _loginFail()
  {
    $this->status = FALSE;
    $this->message = "Đăng nhập thất bại";
    $this->data = "Username hoặc mật khẩu không chính xác";
  }

  private function _validation()
  {
    $rules = [
      [
        'field' => 'user_name',
        'label' => 'User Name',
        'rules' => 'trim|required',
      ],[
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required',
      ]
    ];
    $this->form_validation->set_rules($rules);
    if ($this->form_validation->run() == false)
    {
      $this->status = FALSE;
      $this->message = "Vui lòng kiểm tra lại dữ liệu vừa nhập";
      if (!empty($rules))
      {
        foreach ($rules as $item)
        {
          if(!empty(form_error($item['field']))) $this->data[$item['field']] = form_error($item['field']);
        }
      }
    }
  }
}