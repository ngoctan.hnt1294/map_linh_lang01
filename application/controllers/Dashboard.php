<?php
class Dashboard extends CI_Controller
{
  public $data = [];
  public $message = '';
  public $status = TRUE;

  public function __construct()
  {
    parent::__construct();
    $this->load->library(['form_validation', 'session']);
    $this->load->model('DashboardModel', 'dashboard');
  }

  public function index()
	{
    $this->load->view('dashboard/index');
  }

  public function get_data()
  {
    $data = $this->dashboard->getData();
    if(isset($data) && !empty($data))
    {
      $this->status = TRUE;
      $this->message = "Lây dữ liệu thành công";
      $this->data = $data;
    }
    $this->_response();
  }

  public function get_data_by_id()
  {
    $id = $this->input->post('id');
    $data = $this->dashboard->getDataByID($id);
    if (isset($data) && !empty($data)) {
      $this->status = TRUE;
      $this->message = "Lây dữ liệu thành công";
      $this->data = $data;
    }
    $this->_response();
  }

  public function insert_data()
  {
    if($this->input->method() == 'post') {
      $this->_validation();
      if ($this->status == TRUE) {
        $data = [
          'name' => $this->input->post('name'),
          'address' => $this->input->post('address'),
          'coordinate' => $this->input->post('coordinate'),
          'content' => $this->input->post('content'),
        ];
  
        if(!empty($_FILES['image']['name'])) {
          $upload = $this->_upload_image();
          $data['image_name'] = $upload;
        }
        $this->status = TRUE;
        $this->message = "Thêm dữ liệu thành công";
        $this->data = $data;
        $insert = $this->dashboard->insertData($this->data);
      } 
    }
    $this->_response();
  }

  public function updateData()
  {
    if($this->input->method() == 'post') {
      $this->_validation();
      if ($this->status == TRUE) {
        $data = [
          'name' => $this->input->post('name'),
          'address' => $this->input->post('address'),
          'coordinate' => $this->input->post('coordinate'),
          'content' => $this->input->post('content'),
        ];
        
        if ($this->input->post('remove_img') && file_exists('public/uploads/'.$this->input->post('remove_img'))) {
          unlink('public/uploads/' . $this->input->post('remove_img'));
          $data["image_name"] = '';
        }
  
        if (!empty($_FILES['image']['name'])) {
          $upload = $this->_upload_image();
          $place = $this->dashboard->getDataByID($this->input->post('id'));
          if ($place->image_name !== '' && file_exists('public/uploads/' . $place->image_name)) {
            unlink('public/uploads/' . $place->image_name);
          }
          $data["image_name"] = $upload;
        }
        $is_update = $this->dashboard->updateData(array('id' => $this->input->post('id')), $data);
        if ($is_update == 1) {
          $this->status = TRUE;
          $this->message = "Update thành công";
        } else {
          $this->status = FALSE;
          $this->message = "Update thất bại";
        }
      }
    }
    $this->_response();
  }

  public function delete_data()
  {
    if($this->input->method() == 'post') {
      $id = $this->input->post('id');
      $place = $this->dashboard->getDataByID($id);
      $result = $this->dashboard->deleteData($id);
      if ($result == 1) {
        if (file_exists('public/uploads/' . $place->image_name) && $place->image_name) {
          unlink('public/uploads/' . $place->image_name);
        }
        $this->status = TRUE;
        $this->message = "Xóa thành công";
      }
    }
    $this->_response();
  }

  private function _response()
  {
    echo json_encode(
      [
        'status' => $this->status,
        'message' => $this->message,
        'data' => $this->data
      ],
      JSON_UNESCAPED_UNICODE
    );
  }

  private function _upload_image()
  {
    $config['upload_path'] = 'public/uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['file_name'] = round(microtime(true) * 1000);
    
    $this->load->library('upload', $config);
    
    if (!$this->upload->do_upload('image')){
      $this->status = FALSE;
      $this->message = $this->upload->display_errors();
      $this->data = ["image" => "Upload không hợp lệ"];
      $this->_response();
      exit();
    }
    return $this->upload->data('file_name');
  }

  private function _validation()
  {
    $rules = [
      [
        'field' => 'name',
        'label' => 'Tên địa điểm',
        'rules' => 'trim|required',
      ],[
        'field' => 'address',
        'label' => 'Địa chỉ',
        'rules' => 'trim|required',
      ],[
        'field' => 'coordinate',
        'label' => 'Tọa độ',
        'rules' => 'trim|required',
      ],[
        'field' => 'content',
        'label' => 'Nội dung',
        'rules' => 'trim|required',
      ]
    ];
    $this->form_validation->set_rules($rules);
    if ($this->form_validation->run() == false)
    {
      $this->status = FALSE;
      $this->message = "Vui lòng kiểm tra lại dữ liệu vừa nhập";
      if (!empty($rules))
      {
        foreach ($rules as $item)
        {
          if(!empty(form_error($item['field']))) $this->data[$item['field']] = $item['label'] . ' không được bỏ trống';
        }
      }
    }
  }
}