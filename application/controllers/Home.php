<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  public $data = [];
  public $message = '';
  public $status = TRUE;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('HomeModel', 'home');
  }

	public function index()
	{
		$this->load->view('home/index');
  }
  
  public function get_data()
  {
    $data = $this->home->get_data();
    if(isset($data) && !empty($data))
    {
      $this->status = TRUE;
      $this->message = "Lây dữ liệu thành công";
      $this->data = $data;
    }
    $this->_response();
  }

  private function _response()
  {
    echo json_encode(
      [
        'status' => $this->status,
        'message' => $this->message,
        'data' => $this->data
      ],
      JSON_UNESCAPED_UNICODE
    );
  }
}
