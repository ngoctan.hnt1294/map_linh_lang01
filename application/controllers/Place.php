<?php
class Place extends CI_Controller {
  public $data = [];
  public $message = '';
  public $status = TRUE;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('PlaceModel', 'place');
  }

	public function index()
	{
		$this->load->view('place/index');
  }
  
  public function get_data()
  {
    $data = $this->place->get_data();
    if(isset($data) && !empty($data))
    {
      $this->status = TRUE;
      $this->message = "Lấy dữ liệu thành công";
      $this->data = $data;
    }
    $this->_response();
  }

  private function _response()
  {
    echo json_encode(
      [
        'status' => $this->status,
        'message' => $this->message,
        'data' => $this->data
      ],
      JSON_UNESCAPED_UNICODE
    );
  }
}