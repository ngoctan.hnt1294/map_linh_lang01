import "ol/ol.css";
import { Map, View, Feature } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import { transform } from "ol/proj.js";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import Point from "ol/geom/point";
import { Vector as VectorLayer } from "ol/layer";
import Overlay from "ol/Overlay";

/* ============ create map ============ */

/*
  * show all data
  */
 function showAllData() {
  $.ajax({
    url: base_url + "place/get_data",
    type: "POST",
    success: function (response) {
      let dataAll = JSON.parse(response);
      let status = dataAll.status;
      let data = dataAll.data;
      if (status != false) {
        initMap(data);
      } else {
        console.log("Có lỗi xảy ra");
      }
    },
  });
}

/**
 * Create icon of marker
 */
var iconStyle = new Style({
  image: new Icon({
    anchor: [0.5, 26],
    anchorXUnits: "fraction",
    anchorYUnits: "pixels",
    src:
      "https://raw.githubusercontent.com/jonataswalker/map-utils/master/images/marker_53b3e8.png",
    scale: 0.75
  })
});


/**
 * Create vector layer
 */

function vectorLayer(locations) {
  var vectorLayerList = [];
  let args = [];
  let imgs = [];
  for (let i = 0; i < locations.length; i++) {
    args = locations[i].coordinate.split(/,\s+/);
    if (locations[i]['image_name'] !== '') {
      imgs[i] = '<img src="' + base_url + '/public/uploads/' + locations[i]['image_name'] + '" alt="">';
    } else {
      imgs[i] = '';
    }
    let popupContent = "<div class='ol-popup-content'>";
          popupContent += "<h2 class='ol-popup-content__ttl'>" + locations[i]['name'] + "</h2>";
          popupContent += "<p class='ol-popup-content__address'>" + locations[i]['address'] + "</p>";
          popupContent += "<div class='ol-popup-content__img text-center'>";
            popupContent += imgs[i];
          popupContent += "</div>";
          popupContent += "<p>" + locations[i]['content'] + "</p>";
        popupContent += "</div>";
    let iconFeature = new Feature({
      geometry: new Point(
        transform([args[0], args[1]], "EPSG:4326", "EPSG:3857")
      ),
      name: popupContent
    });
    iconFeature.setStyle(iconStyle);
    let vectorSource = new VectorSource({
      features: [iconFeature]
    });
    let vectorLayer = new VectorLayer({
      source: vectorSource
    });
    vectorLayerList[i] = vectorLayer;
  }
  return vectorLayerList;
}

/**
 * Create map
 */
function initMap(locations) {
  var view = new View({
      center: transform([105.804817, 21.028511], "EPSG:4326", "EPSG:3857"),
      zoom: 12
  });
  
  var map = new Map({
    target: "map",
    layers: [new TileLayer({ source: new OSM() })],
    view: view
  });
  let vectorLayerList = vectorLayer(locations);
  for (let i = 0; i < vectorLayerList.length; i++) {
    map.addLayer(vectorLayerList[i]);
  }
  popupMap(map);
  return map;
}

/* ============ create popup ============ */

// display popup on click
function popupMap(map) {
  var element = document.getElementById("popup");
  var content = document.getElementById('popup-content');
  var closer = document.getElementById('popup-closer');
  var popup = new Overlay({
    element: element,
    positioning: "bottom-center",
    stopEvent: false,
    offset: [0, -50]
  });
  closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
  };
  map.addOverlay(popup);
  map.on("click", function(evt) {
    var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
      return feature;
    });
    if (feature) {
      var featureContent = feature.get('name');
      var coordinates = feature.getGeometry().getCoordinates();
      popup.setPosition(coordinates);
      content.innerHTML = featureContent;
    } else {
      popup.setPosition(undefined);
    }
  });
}

$(function () {
  showAllData();
});