let login_form = $("#login_form")[0];

function login_submit() { 
  let form_data = new FormData(login_form);
  let url = "/login/check_login";
  let ajaxLogin = $.ajax({
    type: "POST",
    url: url,
    data: form_data,
    processData: false,
    contentType: false,
  });

  ajaxLogin.done(function(response){
    let result = JSON.parse(response);
    let data = result.data;
    let status = result.status;
    if (status == false) {
      if($('[name="user_name"]').val() == '') {
        $('[name="user_name"]').siblings('.js-errorAlert').html(data.user_name);
      }
      if($('[name="password"]').val() == '') {
        $('[name="password"]').siblings('.js-errorAlert').html(data.password);
      }
      if ($('[name="user_name"]').val() !== '' && $('[name="password"]').val() !== '') {
        $('.js-failLogin').find('span').text(data);
        $('.js-failLogin').addClass('show');
      }
    } else {
      window.location.href = "/dashboard/";
    }
  });

  ajaxLogin.fail(function (jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ": " + errorThrown);
  });
}

$(function () {
  $(document).on("click", ".js-submitLogin", function(event) {
    event.preventDefault();
    login_submit();
  });

  $(document).on("change", "#user_name, #password", function() {
    $('.js-failLogin').removeClass('show');
    $('.js-errorAlert').empty();
  });
});