let submitMethod;
let form = $("#form_place");

/*
  * Function insert place
  **/
function insertPlace() {
  submitMethod = "insert";
  $(".js-modalTitle").text("Thêm địa điểm");
  $(".js-imgPreview").hide();
  $(".js-alert").empty();
  form[0].reset();
}

/*
  * Function update place
  **/
 function updatePlace(current) {
  let id = $(current).parent("td").find("#id").val();
  submitMethod = "update";
  form[0].reset();
  $(".js-alert").empty();
  $(".js-modalTitle").text("Cập nhật địa điểm");
  let updateAjax = $.ajax({
    type: "POST",
    url: base_url + "dashboard/get_data_by_id",
    data: {
      id: id
    }
  });

  updateAjax.done(function(response){
    let dataAray = JSON.parse(response);
    let status = dataAray.status;
    let data = dataAray.data;

    if(status == true) {
      $('[name="id"]').val(data["id"]);
      $('[name="name"]').val(data["name"]);
      $('[name="address"]').val(data["address"]);
      $('[name="coordinate"]').val(data["coordinate"]);
      $('[name="content"]').val(data["content"]);
      $(".js-imgPreview").show();
      if(data.image_name) {
        $("#imgLabel").text("Change image");
        $(".js-imgStatus").addClass("img-show").html("<figure><img src=\"" + base_url + "public/uploads/" + data.image_name + "\" alt=\"\" class=\"img-responsive\"></figure>");
        $(".js-imgStatus").append("<label><input type=\"checkbox\" name=\"remove_img\" value=\"" + data.image_name + "\"/>Remove photo when saving</label>");
      } else {
        $("#imgLabel").text("Upload image");
        $(".js-imgStatus").text("(No image)");
      }
    }
  });
  updateAjax.fail(function(jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ': ' + errorThrown);
  });
}

/*
  * Function submit form
  **/
 function submitDataPlace() {
  let formData = new FormData(form[0]);
  let url = '';
  $('.js-btnSave').attr('disabled',true);
  if(submitMethod == "insert") {
    url = base_url +  "dashboard/insert_data";
  } else if (submitMethod == "update") {
    url = base_url +  "dashboard/updateData";
  }
  let ajaxCall = $.ajax({
    type: "POST",
    url: url,
    data: formData,
    processData: false,
    contentType: false,
  });

  ajaxCall.done(function (response) {
    let data = JSON.parse(response);
    $('.js-btnSave').attr('disabled',false);
    if (data.status !== false) {
      $("#modal_form").modal("hide");
      showAllPlace();
    } else {
      for (let [key, value] of Object.entries(data.data)) {
        $('[name="'+ key +'"]').parent(".form-group").find(".js-alert").html(value);
      }
    }

  });

  ajaxCall.fail(function (jqXHR, textStatus, errorThrown) {
    console.log(textStatus + ": " + errorThrown);
  });
}

/*
  * Function show all place
  **/
async function showAllPlace() {
  let ajaxShow = await axios.post(base_url + "dashboard/get_data");
  let data  = ajaxShow.data;
  createTable(data.data);
  return data;
}

/*
  * Function delete place
  **/
 function deletePlace(current) {
  let id = $(current).parent("td").find("#id").val();
  if (confirm("Are you sure to delete this user???")) {
    let deleteAjax = $.ajax({
      type: "POST",
      url: base_url + "dashboard/delete_data",
      data: {
        id: id
      }
    });
    deleteAjax.done(function(response) {
      showAllPlace();
    });
    deleteAjax.fail(function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus + ': ' + errorThrown);
    });
  } else {
    return false;
  }
}

/*
  * Function create table
  **/
function createTable(data) {
  let row = [];
  $.each(data, function (index, value) {
    let content = (value.content.match(/\r\n/g) || []).length > 0 ? value.content.replace("\r\n", "<br/>") : value.content;
    if (value.image_name !== '') {
      let imgDir = base_url + "public/uploads/" + value.image_name;
      imgCell = 
        "<figure class=\"user-table__img\">" + 
          "<img src=\"" + imgDir + "\" alt=\"\" class=\"img-responsive\">" + 
        "</figure>";
    } else {
      imgCell = "<p>(No Image)</p>";
    }

    let htmlCode =
      "<tr>" +
        "<td class=\"text-center\">" + value.name + "</td>" +
        "<td class=\"text-center\">" + value.address + "</td>" +
        "<td class=\"text-center\">" + value.coordinate + "</td>" +
        "<td class=\"text-left\"><div class=\"dashboard-table__content js-tableContent\">" + content + imgCell + "</div><a href=\"\" class=\"dashboard-table__more js-readMore\">Xem thêm...</a></td></td>" +
        "<td class=\"text-center\">" +
          "<input type=\"hidden\" id=\"id\" name=\"id\" value=\"" + value.id + "\">" +
          "<a class=\"btn btn-sm btn-primary js-edit\" href=\"javascript:void(0)\" title=\"Edit\" data-toggle=\"modal\" data-target=\"#modal_form\"><i class=\"fas fa-fw fa-edit\"></i> Edit</a>" +
          "<a class=\"btn btn-sm btn-danger js-delete\" href=\"javascript:void(0)\" title=\"Delete\"><i class=\"fa fa-fw fa-trash\"></i> Delete</a>" +
        "</td>" +
      "</tr>";
    row.push(htmlCode);
  });
  $("#js-dashboardTbody").html(row);
}

$(function () {
  showAllPlace();
  $('#dashboard-table').DataTable();

  /* 
   * function click insert button 
   */
  $(document).on("click", ".js-insert", function (event) {
    event.preventDefault();
    insertPlace();
  });

  /* 
   * function click save button 
   */
  $(document).on("click", ".js-btnSave", function (event) {
    event.preventDefault();
    submitDataPlace();
  });

  /* 
   * function click delete button 
   */
  $(document).on("click", ".js-delete", function (event) {
    event.preventDefault();
    deletePlace(this);
  });

  /* 
   * function click edit button 
   */
  $(document).on("click", ".js-edit", function (event) {
    event.preventDefault();
    updatePlace(this);
  });

  /* 
   * function change input
   */
  $(document).on("change", "input", function () {
    $(this).parent(".form-group").find(".js-alert").empty();
  });

  /* 
   * function change textarea
   */
  $(document).on("change", "textarea", function () {
    $(this).parent(".form-group").find(".js-alert").empty();
  });

  /* 
   * function click readmore
   */
  $(document).on("click", ".js-readMore", function (event) {
    event.preventDefault();
    $(this).toggleClass("readLess");
    $(this)
      .closest("td")
      .find(".js-tableContent")
      .toggleClass("dashboard-table__content--more");
    if ($(this).hasClass("readLess")) {
      $(this).text("Ẩn bớt");
    } else {
      $(this).text("Xem thêm...");
    }
  });

  /* 
   * toggle sidebar event
   */
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

  /* 
   * logout event
   */
  $(document).on("click", ".js-logout", function (event) { 
    event.preventDefault();
    $.ajax({
      type: "get",
      url: base_url +  "login/logout/",
      data: {
        action: 'logout'
      },
      success: function (response) {
        window.location.href = base_url + "login/";
      }
    });
  });

  /* 
   * switch page event
   */
  $(document).on('click', '.js-switch', function(event) {
    event.preventDefault();
    let target = $(this).data('target');
    let targetEle = $('#' + target);
    console.log(targetEle.length);
    if (targetEle.length == 0) {
      console.log("có lỗi phát sinh")
    }
    $('.js-container > div').hide();
    targetEle.show();
  });
});