/*
  * show all data
  */
function showAllData() {
  $.ajax({
    url: base_url + "/home/get_data",
    type: "POST",
    success: function (response) {
      let dataAll = JSON.parse(response);
      let status = dataAll.status;
      let data = dataAll.data;
      if (status == true) {
        createList(data);
      }
    },
  });
}

function createList(data) {
  let htmlCode = "<ul class=\"sidebar-list\">";
  let imgDirection = '';
  $.each(data, function (index, value) {
    if (value.image_name !== '') {
      imgDirection = base_url + "/public/uploads/" + value.image_name;
    }
    htmlCode += 
      "<li class=\"sidebar-item\">" +
        "<figure class=\"sidebar-item__img\">" +
          "<img src=\"" + imgDirection + "\" alt=\"\">" +
        "</figure>" +
        "<div class=\"sidebar-item-body\">" +
          "<h4 class=\"sidebar-item__ttl\">" + value.name + "</h4>" +
          "<p class=\"sidebar-item__address mb-0\">" + value.address + "</p>" +
        "</div>"
      "</li>"
  });
  htmlCode += "</ul>";
  $(".js-sidebarContent").html(htmlCode);
}

$(function () {
  showAllData();
});